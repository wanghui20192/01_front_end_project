var month_olypic=[31,29,31,30,31,30,31,31,30,31,30,31];   //闰年
var month_normal=[31,28,31,30,31,30,31,31,30,31,30,31];   //平年
//每个月的名称
var month_name =["January","Febrary","March","April","May","June","July","Auguest","September","October","November","December"];
/*
1。获取当天的年月日
2.根据年判断是闰年还是平年
3.根据是闰年还是平年获得该年的数组
4.根据月先判断该月的第一天是星期几
5.根据月份的天数和该月第一天是星期几来排列日历
6.将今天的日期标记为蓝色，将已经过去的日期标记为灰色，将未标记的日期标记为黑色
7.将结果显示在页面上
*/
//获取今天的日期
var todayDate=new Date();  
//得到年
var year=todayDate.getFullYear();
//得到月，从0开始的
var month=todayDate.getMonth();
//得到日
var day=todayDate.getDate();
//得到星期几
var week=todayDate.getDay();
// alert(year+" "+month+" "+day+" "+week);
/*
判断是闰年还是平年，知道了是闰年还是平年就可以知道了今年每个月有多少天
然后根据该月获取这个月具体有多少天
*/
//能被4整除，但不能被100整除。或者能被400整除的才是闰年
function Day_Month(year,month){
    var temp1=year % 4;
    var temp2=year % 100;
    var temp3=year % 400;
    if((temp1==0 && temp2 !=0)||temp3==0){
        //闰年
        return (month_olypic[month]);
        
    }else{
        return (month_normal[month]);
    }
}
//alert(falg);
//先得到这个月的第一天是星期几
function MonthFirst(year,month){
    var dayFirst=new Date(year,month,1);
    return dayFirst.getDay();
}

/*
执行一个函数，该函数将这个月的第一天放在星期的正下方，然后接下来依次排序，
排序的过程中进行判断，按月份判断如果日期小于今天，字体设置为灰色，今天设
置为蓝色，大于今天的设置为黑色
*/
//该函数用于添加节点，且页面一加载就添加

function appendNote(){

    //用于将li的叠加排序
    var str="";
    var dayul=document.getElementById("calender-day");
    //显示的月份
    var calenderMonth=document.getElementById("calender-month");
    //显示的年份
    var calenderYear=document.getElementById("calender-year");
    //拿到这个月有多少天
    var falg=Day_Month(year,month);
    // 拿到这个月的第一天是星期几
    var oneday=MonthFirst(year,month);
    //将这个月第一天前的全部置为空
    for(var i=0;i<oneday;i++){
        str+="<li class='listyle'>"+""+"</li>";
    }
    //对将下来的日期进行排序
    for(var j=1;j<=falg;j++){
        /*
        过去的日子，颜色设置为灰色,比较日期+月份+年份  
        todayDate.getMonth()为现在月份
        灰色情况：
        1.年份小
        2.年份相同，月份小
        3.年，月份相同，天数小
        */
        if((year<todayDate.getFullYear()) || (year==todayDate.getFullYear() && month<todayDate
        .getMonth()) || (year==todayDate.getFullYear() && month==todayDate
        .getMonth() && j<day)){
            str=str+"<li class='listyle dark'>"+"<span class='lispan'>"+j+"</span>"+"</li>";
        }
        //今天的日子设置为蓝色
        else if((year==todayDate.getFullYear() && month==todayDate
        .getMonth() && j==day)){
            str=str+"<li class='listyle blue'>"+"<span class='lispan'>"+j+"</span>"+"</li>";
        }
        /*
        没过的日子设置为黑色
        */
        else{
            str=str+"<li class='listyle black'>"+"<span class='lispan'>"+j+"</span>"+"</li>";
        }
    }
    //设置日历
    dayul.innerHTML=str;
    //设置月份
    calenderMonth.innerHTML=month_name[month];
    //设置年份
    calenderYear.innerHTML=year;
}
//加载函数用于显示
function show(){
    appendNote();
}

//这个函数用于左选择月份
function clicklt(){
    //year month ，执行月份-1，直到mongth<0时执行年份-1,并将mongth置为11
    month=month-1;
    if(month<0){
        year=year-1;
        month=11;
    }
    appendNote();
}

//这个函数用于右选择月份
function clickgt(){
    month=month+1;
    if(month>11){
        year=year+1;
        month=0;
    } 
    appendNote();
}



